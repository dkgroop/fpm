FROM registry.gitlab.com/dkgroop/workspace:latest 

COPY php-fpm.conf /usr/local/etc/php-fpm.conf
RUN chmod a+r /usr/local/etc/php-fpm.conf

EXPOSE 9000

